## Теоретичні питання

1. **Які способи JavaScript можна використовувати для створення та додавання нових DOM-елементів?**

* document.createElement(tagName): Цей метод створює новий DOM-елемент з вказаним ім’ям тегу.
* element.appendChild(childElement): Цей метод додає дочірній елемент до вказаного батьківського елемента. 
* element.innerHTML = htmlString: Цей метод дозволяє вставити HTML-код безпосередньо в елемент.
* element.insertAdjacentHTML(position, htmlString): Цей метод додає HTML-код вказаного рядка безпосередньо перед або після елемента.
* document.createTextNode(text): Цей метод створює текстовий вузол, який можна додати до елемента.

2. **Опишіть покроково процес видалення одного елементу (умовно клас "navigation") зі сторінки.**

```
const navigationElement = document.querySelector('.navigation');
if (navigationElement) {
  navigationElement.remove();
}

```

3. **Які є методи для вставки DOM-елементів перед/після іншого DOM-елемента?**

* insertAdjacentHTML(position, htmlString): Цей метод дозволяє вставити HTML-код перед або після вказаного елемента. 
* element.insertBefore(newElement, referenceElement): Цей метод додає новий елемент перед вказаним посиланням (referenceElement).
* element.insertAdjacentElement(position, newElement): Цей метод додає новий елемент перед або після вказаного елемента, аналогічно до insertAdjacentHTML(). 