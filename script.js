"use strict";

// task 1

const footer = document.querySelector('footer');

const link = document.createElement('a');
link.textContent = 'Learn More'; 
link.href = '#';

const paragraph = document.querySelector('footer p');
footer.insertBefore(link, paragraph.nextSibling);

// task 2

const mainElement = document.querySelector('main');

const selectElement = document.createElement('select');
selectElement.id = 'rating'; 

const option4 = document.createElement('option');
option4.value = '4';
option4.textContent = '4 Stars';

const option3 = document.createElement('option');
option3.value = '3';
option3.textContent = '3 Stars';

const option2 = document.createElement('option');
option2.value = '2';
option2.textContent = '2 Stars';

const option1 = document.createElement('option');
option1.value = '1';
option1.textContent = '1 Star';

selectElement.appendChild(option4);
selectElement.appendChild(option3);
selectElement.appendChild(option2);
selectElement.appendChild(option1);

const featuresSection = document.querySelector('#features');

mainElement.insertBefore(selectElement, featuresSection);

